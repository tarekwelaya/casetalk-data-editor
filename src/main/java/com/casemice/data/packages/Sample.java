/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casemice.data.packages;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author alisafaya
 */
public class Sample implements Serializable {
    
    public int sampleId;
    public String content;
    
    public Sample(){
        
    }
    
    public Sample(int id, String content){
        this.sampleId = id;
        this.content = content;
    }
    
    public static Sample findSample(List<Sample> list, int Id){
        if (list == null )
            return null;
        Sample ret = null;
        for (Sample item: list) {
            if (Id == item.sampleId) {
                ret = item;
            }
        }
        return ret;
    }
    
    @Override
    public String toString() {
        return this.content;
    }
    
}
