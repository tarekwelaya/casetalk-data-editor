/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casemice.data.packages;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author alisafaya
 */
public class Case implements Serializable {
    
    public String name;
    public int id;
    public Map<Integer,Sample> answers;
    
    public Case(int id, String name,Map<Integer,Sample> answers) {
        this.id = id;
        this.answers = answers;
        this.name = name;
    }
    
    @Override
    public String toString() {
        return this.id + " : "  + this.name;
    }
    
}