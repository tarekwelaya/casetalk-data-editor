/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casemice.data.packages;

import java.io.Serializable;
import java.util.Map;
import java.util.List;


/**
 *
 * @author alisafaya
 */
public class ProjectPackage implements Serializable {
    public Map<Integer,DataClass> dataSet;
    public List<Case> cases;
}
